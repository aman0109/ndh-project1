import React, {
    useState,
    forwardRef,
    useImperativeHandle,
    createRef,
} from "react";

import "../components/LoginForm/login.css";
import ListUser from "./Sample/ListUser";

import { useHistory } from "react-router-dom";

const HRUserListPage = forwardRef((props, ref) => {
    let history = useHistory();
    const child = createRef();

    useImperativeHandle(ref, () => ({
        foo(data) {
            child.current.foo(data);
        },
    }));

    return (
        <div>
            <div className="d-flex justify-content-center">
                <img
                    src="https://www.ndhgo.com/wp-content/uploads/2021/05/NDHGO-Logo-2021-01-1024x339.png"
                    style={{ width: 185 }}
                    alt="logo"
                />
            </div>

            <div>
                <button
                    style={{ float: "right" }}
                    name="AddUsers"
                    type="button"
                    className="btn btn-lg gradient-custom-2 mx-4 flex-end"
                    onClick={props.toggle}
                >
                    Add Users
                </button>
            </div>
            <br />

            <br />

            <div className="container my-4">
                <ListUser userState={props.userState} ref={child} />
            </div>
        </div>
    );
});

export default HRUserListPage;
