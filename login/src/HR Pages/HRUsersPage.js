import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import "../components/LoginForm/login.css";
import PostData from "../Validation/PostData";

const HRUsersPage = (props) => {
    //console.log(user);
    const handle = () => {};
    const history = useHistory();
    const [isDisabled, setDisabled] = useState(false);
    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        watch,
        reset,
    } = useForm({
        defaultValues: {
            uname: "",
            name: "",
            email: "",
            role: [],
            token: "",
            mobileNo: "",
        },
    });
    const token = watch("token");

    const onSubmit = (data) => {
        const addUsersApiObj = {
            // username: data.uname,
            fname: data.fname,
            email: data.email,
            lname: data.lname,
        };

        PostData("http://localhost:8080/api/v1/employees", addUsersApiObj)
            .then((data) => {
                reset();
                setDisabled(false);
                console.log(data);
                props.func(addUsersApiObj);
            })
            .catch(() => {});
    };

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <section
                    className="h-100 gradient-form"
                    style={{ backgroundColor: "#eee" }}
                >
                    <div className="container py-5 h-100">
                        <div className="row d-flex justify-content-center h-100">
                            <div>
                                <div>
                                    <div>
                                        <div>
                                            <div className="text-center">
                                                <img
                                                    src="https://www.ndhgo.com/wp-content/uploads/2021/05/NDHGO-Logo-2021-01-1024x339.png"
                                                    style={{ width: 185 }}
                                                    alt="logo"
                                                />
                                            </div>

                                            <p className="header">
                                                Enter The Credentials
                                            </p>
                                            <br />

                                            <div className="mb-3">
                                                <label
                                                    htmlFor="exampleFormControlInput1"
                                                    className="header"
                                                >
                                                    First Name
                                                </label>
                                                <input
                                                    className="form-control"
                                                    type="text"
                                                    placeholder="Enter Your name"
                                                    name="fname"
                                                    {...register("fname", {
                                                        required: true,
                                                        maxLength: 30,
                                                    })}
                                                />
                                                {errors.name &&
                                                    errors.name.type ===
                                                        "required" && (
                                                        <div className="text-danger">
                                                            required
                                                        </div>
                                                    )}
                                                {errors.name &&
                                                    errors.name.type ===
                                                        "maxLength" && (
                                                        <div className="text-danger">
                                                            Max length exceeded
                                                        </div>
                                                    )}
                                            </div>

                                            <div className="mb-3">
                                                <label
                                                    htmlFor="exampleFormControlInput1"
                                                    className="header"
                                                >
                                                    Last Name
                                                </label>
                                                <input
                                                    className="form-control"
                                                    type="text"
                                                    placeholder="Enter Your name"
                                                    name="lname"
                                                    {...register("lname", {
                                                        required: true,
                                                        maxLength: 30,
                                                    })}
                                                />
                                                {errors.name &&
                                                    errors.name.type ===
                                                        "required" && (
                                                        <div className="text-danger">
                                                            required
                                                        </div>
                                                    )}
                                                {errors.name &&
                                                    errors.name.type ===
                                                        "maxLength" && (
                                                        <div className="text-danger">
                                                            Max length exceeded
                                                        </div>
                                                    )}
                                            </div>

                                            <div className="mb-3">
                                                <label
                                                    htmlFor="exampleFormControlInput1"
                                                    className="header"
                                                >
                                                    Select Your Role
                                                </label>
                                                <select
                                                    className="form-control"
                                                    name="role"
                                                    {...register("role", {
                                                        required: true,
                                                    })}
                                                >
                                                    <option
                                                        value=""
                                                        style={{
                                                            color: "gray",
                                                        }}
                                                    >
                                                        Select Your Role
                                                    </option>
                                                    <option value="admin">
                                                        Admin
                                                    </option>
                                                    <option value="moderator">
                                                        Moderator
                                                    </option>
                                                    <option value="user">
                                                        {" "}
                                                        User
                                                    </option>
                                                </select>
                                                {errors.role &&
                                                    errors.role.type ===
                                                        "required" && (
                                                        <div className="text-danger">
                                                            required
                                                        </div>
                                                    )}
                                            </div>

                                            <div className="mb-3">
                                                <label
                                                    htmlFor="exampleFormControlInput1"
                                                    className="header"
                                                >
                                                    Email
                                                </label>
                                                <input
                                                    className="form-control"
                                                    type="text"
                                                    name="email"
                                                    placeholder="name@example.com"
                                                    {...register("email", {
                                                        required: true,
                                                        pattern: new RegExp(
                                                            /\S+@\S+\.\S+/
                                                        ),
                                                    })}
                                                />
                                                {errors.email &&
                                                    errors.email.type ===
                                                        "required" && (
                                                        <div className="text-danger">
                                                            required
                                                        </div>
                                                    )}
                                                {errors.email &&
                                                    errors.email.type ===
                                                        "pattern" && (
                                                        <div className="text-danger">
                                                            Enter valid email
                                                        </div>
                                                    )}
                                            </div>

                                            <div className="text-center pt-1 mb-5 pb-1">
                                                <button
                                                    onClick={handle}
                                                    className="btn btn-primary p-3 btn-block fa-lg gradient-custom-2 mb-3"
                                                    type="submit"
                                                    value="Submit"
                                                    style={{
                                                        fontFamily: "Verdana",
                                                        textAlign: "center",
                                                        fontSize: 15,
                                                        color: "black",
                                                    }}
                                                >
                                                    Add User
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
        </>
    );
};

export default HRUsersPage;
