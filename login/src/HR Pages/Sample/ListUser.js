import React, {
    useEffect,
    useState,
    forwardRef,
    useImperativeHandle,
} from "react";
import SampleUser from "./SampleUser";

// List of all users...
async function fetchAllUsers() {
    const response = await fetch("http://localhost:8080/api/v1/employees");
    const users = await response.json();
    return users;
}
// Delete user...
async function deleteUserFromDatabase(id) {
    if (window.confirm("Are You Sure You Want To Delete The User")) {
        const response = await fetch(
            "http://localhost:8080/api/v1/employees/" + id,
            {
                method: "DELETE",
            }
        );
    }
}

const ListUser = forwardRef((props, ref) => {
    const [user, setUser] = useState([]);
    const [error, setError] = useState("");

    useEffect(() => {
        fetchAllUsers()
            .then((data) => {
                setUser(data);
                setError("");
            })
            .catch((error) => {
                setError(error);
            });
    }, []);

    useImperativeHandle(ref, () => ({
        foo(data) {
            user.splice(0, 0, data);
            setUser([...user]);
        },
    }));

    const removeUser = (id) => {
        console.log(id);
        deleteUserFromDatabase(id).then(() => {
            const index = user.findIndex((item) => item.id === id);
            user.splice(index, 1);
            setUser([...user]);
        });
    };

    return (
        <div>
            {user.length > 0 ? (
                user.map((item, index) => (
                    <SampleUser
                        objects={item}
                        key={index}
                        onDelete={removeUser}
                    />
                ))
            ) : (
                <div style={{ backgroundColor: "grey" }} className="py-2">
                    <p
                        style={{
                            fontFamily: "muller",
                            fontSize: "30px",
                            fontWeight: "300",
                        }}
                    >
                        No Users to display
                    </p>
                </div>
            )}
        </div>
    );
});

export default ListUser;
