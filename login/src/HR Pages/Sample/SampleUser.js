import React from "react";
import { BsFillCheckCircleFill } from "react-icons/bs";
import { GrEdit } from "react-icons/gr";
import { MdDelete } from "react-icons/md";
import "../../utils/EmployeeService";
import { useHistory } from "react-router-dom";

function SampleUser(props) {
    let history = useHistory();
    const editEmployee = (e) => {
        e.preventDefault();
        //navigate(`/editEmployee/${id}`);

        history.push(`/editEmployee/${props.objects.id}`);
    };
    return (
        <div>
            <div className="card my-3">
                <div className="card-header">
                    <div className="d-flex">
                        <div className="flex-fill">
                            <p className="h4">
                                <strong>User Id:</strong>
                                {props.objects.id}
                            </p>
                        </div>
                        <div className="d-flex justify-content-center align-items-center">
                            <GrEdit
                                className="m-2"
                                onClick={(e) => editEmployee(e)}
                            />
                            <MdDelete
                                size={20}
                                className="m-2"
                                color="#d11a2a"
                                onClick={() => props.onDelete(props.objects.id)}
                            />
                        </div>
                    </div>
                </div>
                <div className="card-body">
                    <div className="d-flex">
                        <div className="flex-fill">
                            <p>
                                <strong>First Name: </strong>
                                {props.objects.fname}
                            </p>
                            <p>
                                <strong>Last Name:</strong>{" "}
                                {props.objects.lname}
                            </p>
                            <p>
                                <strong>Email:</strong> {props.objects.email}
                            </p>
                            <p>
                                <strong>
                                    {/* Roles. - {props.objects.roles[0].name} */}
                                </strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SampleUser;
