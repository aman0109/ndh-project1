import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import EmployeeService from "../../utils/EmployeeService";
import { useHistory } from "react-router-dom";

const UpdateEmployee = () => {
    const { id } = useParams();
    const history = useHistory();
    const [employee, setEmployee] = useState({
        id: id,
        fname: "",
        lname: "",
        email: "",
    });

    const handleChange = (e) => {
        const value = e.target.value;
        setEmployee({ ...employee, [e.target.name]: value });
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await EmployeeService.getEmployeeById(
                    employee.id
                );
                setEmployee(response.data);
            } catch (error) {
                console.log(error);
            }
        };
        fetchData();
    }, []);

    const updateEmployee = (e) => {
        e.preventDefault();
        console.log(employee);
        EmployeeService.updateEmployee(employee, id)
            .then((response) => {
                history.push("/dashboard");
            })
            .catch((error) => {
                console.log(error);
            });
    };

    return (
        <>
            <section style={{ backgroundColor: "#508bfc" }}>
                <div className="container py-5 h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col-12 col-md-8 col-lg-6 col-xl-5">
                            <div
                                className="card shadow-2-strong"
                                // style="border-radius: 1rem;"
                                style={{ borderRadius: "1rem" }}
                            >
                                <div className="card-body p-5 text-center">
                                    <div className="text-center">
                                        <img
                                            src="https://www.ndhgo.com/wp-content/uploads/2021/05/NDHGO-Logo-2021-01-1024x339.png"
                                            style={{ width: 185 }}
                                            alt="logo"
                                        />
                                    </div>

                                    <h3 className="mb-5">Update User {id}</h3>

                                    <div className="form-outline mb-4">
                                        <div>
                                            <label
                                                style={{
                                                    float: "left",
                                                    fontSize: "20px",
                                                    fontWeight: "500",
                                                    fontFamily: "muller",
                                                }}
                                                className="form-label mx-3"
                                                for="typeEmailX-2"
                                            >
                                                First Name
                                            </label>
                                        </div>
                                        <input
                                            className="form-control form-control-lg"
                                            type="text"
                                            name="fname"
                                            value={employee.fname}
                                            onChange={(e) => handleChange(e)}
                                        ></input>
                                    </div>

                                    <div className="form-outline mb-4">
                                        <div>
                                            <label
                                                style={{
                                                    float: "left",
                                                    fontSize: "20px",
                                                    fontWeight: "500",
                                                    fontFamily: "muller",
                                                }}
                                                className="form-label"
                                                for="typePasswordX-2"
                                            >
                                                Last Name
                                            </label>
                                        </div>
                                        <input
                                            className="form-control form-control-lg"
                                            type="text"
                                            name="lname"
                                            value={employee.lname}
                                            onChange={(e) => handleChange(e)}
                                        ></input>
                                    </div>

                                    <div className="form-outline mb-4">
                                        <div>
                                            <label
                                                style={{
                                                    float: "left",
                                                    fontSize: "20px",
                                                    fontWeight: "500",
                                                    fontFamily: "muller",
                                                }}
                                                className="form-label"
                                                for="typePasswordX-2"
                                            >
                                                Email
                                            </label>
                                        </div>
                                        <input
                                            type="email"
                                            name="email"
                                            value={employee.email}
                                            onChange={(e) => handleChange(e)}
                                            className="form-control form-control-lg"
                                        ></input>
                                    </div>

                                    <button
                                        onClick={updateEmployee}
                                        className="btn btn-lg  btn-primary px-5 mx-2"
                                        type="submit"
                                        value="Submit"
                                    >
                                        Update
                                    </button>

                                    <button
                                        onClick={() =>
                                            history.push("/dashboard")
                                        }
                                        className="btn btn-lg btn-block btn-danger "
                                        type="submit"
                                        value="Submit"
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

export default UpdateEmployee;
