import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import "../components/LoginForm/login.css";
import PostData from "../Validation/PostData";
import IPAddress from "../Validation/IPAddress";

const HRLoginPage = () => {
    const history = useHistory();
    const [error, setError] = useState("");
    const {
        register,
        handleSubmit,
        formState: { errors },
        watch,
    } = useForm();
    const watchRole = watch("role");
    const accessToken = localStorage.getItem("accessToken");
    console.log(accessToken);
    useEffect(() => {
        // if (accessToken !== null) {
        //     history.push("/dashboard");
        // }
    });

    async function LoginApi(LoginApiObj) {
        const settings = {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(LoginApiObj),
        };
        // let fetchResponse = null;
        let fetchResponse = await fetch(
            IPAddress + "/api/auth/signin",
            settings
        );
        console.log(fetchResponse.status);
        // try {

        //     console.log(fetchResponse);
        // } catch (error) {
        //     console.log(error);
        //     setError("Server is not responding");
        // }
        return fetchResponse;
    }

    const onSubmit = (data) => {
        const LoginApiObj = {
            role: ["admin"],
            user_name: data.name,
            password: data.password,
        };
        // PostData("http://10.1.30.18:9032/api/auth/signin", LoginApiObj)
        //     .then((data) => {
        //         console.log(data);
        //         history.push("/dashboard");
        //     })
        //     .catch((error) => console.log(error));
        LoginApi(LoginApiObj).then(async (response) => {
            if (response !== null) {
                if (response.ok === true) {
                    const user = await response.json();
                    console.log(user);
                    //localStorage.setItem("token", user.accessToken);
                    history.push("/dashboard");
                } else {
                    const errorObj = await response.json();
                    setError(errorObj.message);
                }
            }
        });
    };

    return (
        <>
            {error !== "" ? (
                <div
                    className="alert alert-warning alert-dismissible fade show"
                    role="alert"
                >
                    <strong>{error}</strong>
                    <button
                        type="button"
                        className="close"
                        data-dismiss="alert"
                        aria-label="Close"
                        onClick={() => setError("")}
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            ) : null}
            <form onSubmit={handleSubmit(onSubmit)}>
                <section
                    className="h-100 gradient-form"
                    style={{ backgroundColor: "#eee" }}
                >
                    <div className="container py-5 h-100">
                        <div className="row d-flex justify-content-center align-items-center h-100">
                            <div className="col-xl-10">
                                <div className="card rounded-3 text-black">
                                    <div className="row g-0">
                                        <div className="col-lg-6">
                                            <div className="card-body p-md-5 mx-md-4">
                                                <div className="text-center">
                                                    <img
                                                        src="https://www.ndhgo.com/wp-content/uploads/2021/05/NDHGO-Logo-2021-01-1024x339.png"
                                                        style={{ width: 185 }}
                                                        alt="logo"
                                                    />
                                                </div>

                                                <p className="header">
                                                    Enter Your Credentials
                                                </p>
                                                <br />

                                                <div className="mb-3">
                                                    <label
                                                        htmlFor="exampleFormControlInput1"
                                                        className="header"
                                                    >
                                                        User Name
                                                    </label>
                                                    <input
                                                        className="form-control"
                                                        type="text"
                                                        placeholder="username"
                                                        name="name"
                                                        {...register("name", {
                                                            required: true,
                                                            maxLength: 30,
                                                        })}
                                                    />
                                                    {errors.name &&
                                                        errors.name.type ===
                                                            "required" && (
                                                            <div className="text-danger">
                                                                required
                                                            </div>
                                                        )}
                                                    {errors.name &&
                                                        errors.name.type ===
                                                            "maxLength" && (
                                                            <div className="text-danger">
                                                                Max length
                                                                exceeded
                                                            </div>
                                                        )}
                                                </div>

                                                {/* 
<div className="mb-3">
  <label htmlFor="exampleFormControlInput1" className="header">Email address</label>
  <input  className="form-control" type="text" name="email"  placeholder="name@example.com" {...register('email', { required: true, pattern:new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)})}/>
{errors.email && errors.email.type === "required" && (
<div className='text-danger'>required</div>
)}
{errors.email && errors.email.type === "pattern" && (
<div className='text-danger'>Enter valid email</div>
)}
  
</div> */}

                                                {/* <div className="mb-3">
  <label htmlFor="exampleFormControlInput1" className="header">Phone Number</label>
  <input  className="form-control" type="text" name="MobNo"  placeholder="Enter your 10 digit phone number" 
  {...register('MobNo', { required: true, minLength: 10, maxLength:10, pattern:new RegExp(/^[0-9\b]+$/)})}/>
  {errors.MobNo && errors.MobNo.type === "required" && (
   <div  className='text-danger'>required</div>
  )}
  {errors.MobNo && errors.MobNo.type === "minLength" && (
  <div className='text-danger'>Min length should be 10</div>
  )} 
  {errors.MobNo && errors.MobNo.type === "maxLength" && (
  <div className='text-danger'>Max length should be 10</div>
  )} 
  {errors.MobNo && errors.MobNo.type === "pattern" && (
  <div className='text-danger'>Enter Valid mobile number</div>
  )} 
  
</div> */}

                                                {/* <div className="mb-3">
  <label htmlFor="exampleFormControlInput1" className="header" >Select Your Role</label>
  <select  className="form-control"  name="role"  {...register('role', { required: true })} >
    <option value="" style={{color:"gray"}} >Select Your Role</option>
    <option value="admin">Admin</option>
     <option value="moderator">Moderator</option> 
    <option value="user"> User</option> 
  </select>
  {errors.role && errors.role.type === "required" && (
        <div  className='text-danger'>required</div>
      )} 

  </div> */}

                                                <div className="mb-3">
                                                    <label
                                                        htmlFor="exampleFormControlInput1"
                                                        className="header"
                                                    >
                                                        Select Your Role
                                                    </label>
                                                    <select
                                                        className="form-control"
                                                        name="role"
                                                        {...register("role", {
                                                            required: true,
                                                        })}
                                                    >
                                                        <option
                                                            value=""
                                                            style={{
                                                                color: "gray",
                                                            }}
                                                        >
                                                            Select Your Role
                                                        </option>
                                                        <option value="HR">
                                                            HR
                                                        </option>
                                                        <option value="moderator">
                                                            Moderator
                                                        </option>
                                                    </select>
                                                    {errors.role &&
                                                        errors.role.type ===
                                                            "required" && (
                                                            <div className="text-danger">
                                                                required
                                                            </div>
                                                        )}
                                                </div>

                                                <div className="mb-3">
                                                    <label
                                                        htmlFor="exampleFormControlInput1"
                                                        className="header"
                                                    >
                                                        Password
                                                    </label>
                                                    <input
                                                        className="form-control"
                                                        type="password"
                                                        name="password"
                                                        placeholder="Enter your password"
                                                        {...register(
                                                            "password",
                                                            { required: true }
                                                        )}
                                                    />
                                                    {errors.password &&
                                                        errors.password.type ===
                                                            "required" && (
                                                            <div className="text-danger">
                                                                required
                                                            </div>
                                                        )}
                                                </div>

                                                <div className="text-center pt-1 mb-5 pb-1">
                                                    <button
                                                        className="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3"
                                                        type="submit"
                                                        value="Submit"
                                                        style={{
                                                            fontFamily:
                                                                "Verdana",
                                                            textAlign: "center",
                                                            padding: 5,
                                                        }}
                                                    >
                                                        Sign in
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-6 d-flex align-items-center gradient-custom-2">
                                            <div className="text-white px-3 py-4 p-md-5 mx-md-4">
                                                <h4 className="extraHead">
                                                    India's No-1 App To Sell
                                                    Online
                                                </h4>
                                                <br></br>
                                                <br></br>
                                                <p className="extraHead2">
                                                    NDHGO platform enables any
                                                    business small or large to
                                                    go online in less than a
                                                    minute. NDHGO aims to
                                                    provide online store to
                                                    India's 60 Million small
                                                    businesses.
                                                    <br />
                                                    The platform allows you to
                                                    set up and run your online
                                                    store and empowers you to
                                                    compete with the e-commerce
                                                    giants. You can create your
                                                    online store from your phone
                                                    with a professional-looking
                                                    product catalog. You can
                                                    share a link of your store
                                                    easily with your customers
                                                    through trending Social
                                                    Media platforms and
                                                    WhatsApp. If you are an
                                                    individual entrepreneur and
                                                    urge to sell your products
                                                    online, then your best and
                                                    wise choice would be NDHGO,
                                                    which is a free platform!
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
        </>
    );
};

export default HRLoginPage;
