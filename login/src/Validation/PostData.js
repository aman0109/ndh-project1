async function PostData(str, obj) {
    const settings = {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            //'accessToken':localStorage.getItem('accessToken')
        },
        body: JSON.stringify(obj),
    };
    try {
        const fetchResponse = await fetch(str, settings);
        const data = await fetchResponse.json();
        return data;
    } catch (e) {
        return e;
    }
}

export default PostData;
