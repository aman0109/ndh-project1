import React, { useRef, useState, createRef } from "react";
//import MainPage from './components/MainPage.js'
import SlideDrawer from "./slideDrawer.js";
import Backdrop from "./Backdrop.js";
import HRUserListPage from "../HR Pages/HRUserListPage";
// export default class MainSideDrawer extends React.Component {
//    state = { drawerOpen: false }
// drawerToggleClickHandler = () => {
//     this.setState({
//       drawerOpen: !this.state.drawerOpen
//     })
//   }
// backdropClickHandler = () => {
//     this.setState({
//       drawerOpen: false
//     })
//   }
//    render(){
//       let backdrop;
//       if(this.state.drawerOpen){
//         backdrop = <Backdrop close={this.backdropClickHandler}/>;
//        }
//       return(

//          <div>
//            < SlideDrawer show={this.state.drawerOpen} />
//            { backdrop }
//            < HRUserListPage toggle={this.drawerToggleClickHandler} />
//          </div>
//       )
//     }
// }

function MainSideDrawer() {
    const [drawerOpen, setdrawer] = useState(false);
    const [user, setUser] = useState(null);
    const drawerToggleClickHandler = () => {
        setdrawer(!drawerOpen);
    };
    const backdropClickHandler = () => {
        setdrawer(false);
    };
    let backdrop;
    const childFunc = createRef();
    if (drawerOpen) {
        backdrop = <Backdrop close={backdropClickHandler} />;
    }
    let value = null;
    const pullData = (data) => {
        childFunc.current.foo(data);
    };

    return (
        <div>
            <SlideDrawer show={drawerOpen} func={pullData} />
            {backdrop}
            <HRUserListPage
                toggle={drawerToggleClickHandler}
                userState={childFunc}
                ref={childFunc}
            />
        </div>
    );
}

export default MainSideDrawer;
