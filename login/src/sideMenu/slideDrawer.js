// import React from 'react'
// import HRUsersPage from '../HR Pages/HRUsersPage'
// import './slideDrawer.css'
// const SlideDrawer = (props) => {
//     let drawerClasses = 'side-drawer'
//     if(props.show) {
//         drawerClasses = 'side-drawer open'
//      }

//   return (
//     <div>
//         <HRUsersPage/>
//     </div>
//   )
// }

// export default SlideDrawer

import React from "react";
import HRUsersPage from "../HR Pages/HRUsersPage";
import "./slideDrawer.css";
// export default class SlideDrawer extends React.Component {
//    render(){
//        let drawerClasses = 'side-drawer'
//        if(this.props.show) {
//           drawerClasses = 'side-drawer open'
//        }
//        return(

//           <div className={drawerClasses}>
//              <HRUsersPage/>
//           </div>
// )
//     }

// }

function SlideDrawer(props) {
    let drawerClasses = "side-drawer";
    if (props.show) {
        drawerClasses = "side-drawer open";
    }
    return (
        <div className={drawerClasses}>
            <HRUsersPage func={props.func} />
        </div>
    );
}

export default SlideDrawer;
