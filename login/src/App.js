import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import HRUsersPage from "./HR Pages/HRUsersPage";
import MainSideDrawer from "./sideMenu/mainSideDrawer";

import UpdateEmployee from "./HR Pages/Sample/UpdateEmployee";

function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/dashboard" component={MainSideDrawer} />
                <Route exact path="/HRUsersPage" component={HRUsersPage} />
                <Route
                    exact
                    path="/editEmployee/:id"
                    component={UpdateEmployee}
                />
            </Switch>
        </Router>
    );
}

export default App;
