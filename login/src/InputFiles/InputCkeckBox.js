import React from 'react'

function InputCkeckBox(props) {

    const { formState, label, name, validation } = props
    const { register } = formState
    return (
        <div className="form-check">
            <input
                className="form-check-input"
                type="checkbox"
                value={label}
                name={name}
                id={label}
                {...register(`${name}`, validation)} />
            <label className="form-check-label" htmlFor={label}>{label}</label>

        </div>
    )
}

export default InputCkeckBox