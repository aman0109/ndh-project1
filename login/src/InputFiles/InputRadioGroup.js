import React from 'react'
import Validation from '../Validation/Validation'
import InputRadio from './InputRadio'

function InputRadioGroup(props) {

    const { formState: { errors } } = props.formState
    const validation = Validation().validationDegree
    const radiogroups = props.label
    const name = props.name
    const labelGroup = props.labelGroup

    return (
        <div>
            {labelGroup ? <label className="mr-4 font-weight-bold">{labelGroup}</label> : null}
            {radiogroups.map((radioItems) =>
                <InputRadio
                    key={radioItems}
                    formState={props.formState}
                    label={radioItems}
                    name={name}
                    validation={validation}
                ></InputRadio>
            )}
            <div className="text-danger" ><small>{errors[name]?.message}</small></div>
        </div>
    )
}
export default InputRadioGroup