import React from 'react'


function TextAreaInput(props) {

    const { formState, label, name, validation } = props
    const { register, formState: { errors } } = formState
    return (
        <div className="form-group">
            {label === null ? null : <label htmlFor={name} className="form-label">{label}</label>}
            <textarea
                type="text"
                className="form-control"
                id={name}
                {...register(`${name}`, validation)}

            />
            <div className="text-danger" >{errors[name]?.message}</div>

        </div>
    )
}

export default TextAreaInput