import React from 'react'

function InputRadio(props) {

    const { formState, label, name, validation } = props
    const { register } = formState
    return (
        <div className="form-check form-check-inline">
            <input
                className="form-check-input"
                type="radio"
                value={label}
                {...register(`${name}`, validation)}

            />
            <label className="form-check-label" htmlFor="male">{label}</label>
        </div>
    )
}

export default InputRadio