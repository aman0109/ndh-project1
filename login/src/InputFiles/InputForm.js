import React from 'react'

function InputForm(props) {

    const { formState, label, name, validation } = props
    const { register, formState: { errors } } = formState
    return (
        <div>
            {label ? <label className='form-label' >{label}</label> : null}
            <input
                type="text"
                id={name}
                className="form-control"
                {...register(`${name}`, validation)}
            />
            <div className="text-danger" ><small>{errors[name]?.message}</small></div>
        </div>
    )
}
export default InputForm