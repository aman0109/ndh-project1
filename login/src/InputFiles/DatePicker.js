import React from 'react'

function DatePicker(props) {

    const dateToday=()=>
    {
      var today,dd,mm,yyyy;
      today=new Date();
      dd=today.getDate()
      mm=today.getMonth()+1
      yyyy=today.getFullYear()
  
      if(dd<10)
      {
        dd='0'+dd
      }
  
      if(mm<10)
      {
        mm='0'+mm
      }
      var curDate=yyyy+"-"+mm+"-"+dd
    
  
  
      return curDate
  
    }
    const { formState, label, name, validation, type } = props
    const { register, formState: { errors } } = formState

    const min = type === 'date' ?  "1950-01-01":""
    const max= type === 'date' ?  dateToday():""
    return (
        <div>
            <div>
                {label ? <label className='form-label' >{label}</label> : null}
                <input
                    type={type}
                    id={name}
                    min={min}
                    max={max}

                    
                    className="form-control"
                    {...register(`${name}`, validation)}
                />
                <div className="text-danger" ><small>{errors[name]?.message}</small></div>
            </div>
        </div>
    )
}

export default DatePicker